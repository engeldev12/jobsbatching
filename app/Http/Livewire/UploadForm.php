<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UploadForm extends Component
{
    public $name;
    public $file;

    public function upload()
    {
        # code...
    }

    public function render()
    {
        return view('livewire.upload-form');
    }
}
