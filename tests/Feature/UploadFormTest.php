<?php

namespace Tests\Feature;

use App\Http\Livewire\UploadForm;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class UploadFormTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function puedoVerElFormParaCargarArchivos()
    {
        $response = $this->get('/');
        $response->assertSeeLivewire('upload-form');
    }
    

    /** @test */
    public function elComponenteTieneSusPropiedadesBindeadas()
    {
        Livewire::test(UploadForm::class)
            ->assertPropertyWired('name')
            ->assertPropertyWired('file')
            ->assertMethodWiredToForm('upload');
    }
}
