<div>
    <div class="row mt-4">
        <div class="col-12">
            <article class="card">
                <div class="card-body">
                    <h4 class="card-title">Carga de migración</h4>
                    <form wire:submit.prevent="upload" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre del archivo</label>
                            <input type="text" wire:model="name" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="file">Archivo a cargar</label>
                            <input type="file" wire:model="file" id="file" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Cargar</button>
                    </form>
                </div>
            </article>
        </div>
    </div>
</div>
